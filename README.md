# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)

## Dependencies

### TypeScript

`npm install --save typescript @types/node @types/react @types/react-dom @types/jest`

# or

`yarn add typescript @types/node @types/react @types/react-dom @types/jest`

## Docker

### Dockerfile

### couches / layers des images docker 

+ 2 types :
  + en lecture seule
  + lecture / écriture

+ couches peuvent se partager entre les images
  + utilise le cache
  + meilleur      temps de chargement

    illustration:
|image 1     |     image 2| 
couche 1          couche A
couche 2          couche B
          couche 3
couche 4          couche D

les images ont des couches différentes & des couches partagées :

+ écriture :

|conteneur 1     |     conteneur 2| 
R   couche 1              couche A
R   couche 2              couche B
R            couche 3
R   couche 4              couche D
RW  couche 5              couche E

+ on travail sur la conception, par exemple en regroupant ici `git` et `vim` en 1 seul `run`
+ Dockerfile, empilement par couches

```yml
FROM ubuntu:latest
RUN apt-get update apt-get install -y --no-install-recommends vim git
```
+ Résultat avec un `docker history` : 
  + on peut voir le gain de couches

+ En mode écriture avec un run

```yml
docker run -tid --name test hugues:v1.0
docker exec -ti test sh
touch heyoh
rm -rf srv/
```
+ que se passe-t-il?

```s
  -$ docker diff test
A /heyoh
D /srv
```
+ NS SOMMES DS LA MEME LOGIQUE QUE GIT
  `docker commit`

### Docker layers et Microservices

+ INTERETS  

### CI/CD on GitLab

+ create a `.gitlab-ci.yml` file

### React Query

+ i cmd :
`yarn add react-query`

### Styled-components

+ install cmd :
`yarn add styled-components`

### Storybook

+ install cmd :
`npx sb init`

## Test

+ Jest

+ React testing library :

## EsLint

### SCSS


