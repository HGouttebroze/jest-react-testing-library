# TypeScript 

+ langage fortement typé qui permet d'avoir un code plus structuré et plus facile à maintenir

+ possibilité de typer de manière statiques les variables et les fonctions. 

+ Le code peut être ensuite convertit en JavaScript en retirant les déclarations liées aux types.

## gérer les types complexes 

## créer des fichiers de déclaration 

## alias

```ts
type DateString = string
type Id = string | number
type EventListener = (e: Event) => void


function push<T, U>(items: T[], item: U): (T | U)[] {
    return [...items, item]
}
```