import { Todo } from "./Interfaces";

export default class TodoStore {
  public todos: Todo[];

  private static i = 0;

  /**
   * auto-increment system
   */
  private static increment() {
    return this.i++;
  }

  /**
   * warning to the state of JavaScript objet
   * never change a state value because
   * Mutation are not alow !!!
   * @param title
   */
  addTodo(title: string): void {
    this.todos = [
      {
        id: TodoStore.increment(),
        title: title,
        completed: false,
      },
      ...this.todos,
    ];
  }

  removeTodo(todo: Todo): void {
    this.todos = this.todos.filter((t) => t !== todo);
  }

  updateTodo(todo: Todo, title: string): void {
    this.todos = this.todos.map((t) => (t === todo ? { ...t, title } : t));
  }

  deleteTodo(): void {
    this.todos = this.todos.filter((t) => !t.completed);
  }

  toggleTodo(todo: Todo, completed = true): void {
    this.todos = this.todos.map((t) => (t === todo ? { ...t, completed } : t));
  }

  /**
   * change 'completed value only when different of the todo
   */
  toggleAll(completed = true) {
    this.todos = this.todos.map((t) =>
      completed !== t.completed ? { ...t, completed } : t
    );
  }
}
