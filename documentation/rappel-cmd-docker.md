# DOCKER

## install
see info cmd install :

`sudo apt-cache search docker.io`

+ remove all docker versions installed :

`sudo apt-get remove docker docker-engine docker.io containerd runc`

+ next : 

` sudo apt-get update`
 `sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release`

+ Add Docker’s official GPG key:

 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

+ Use the following command to set up the stable repository. To add the nightly or test repository, add the word nightly or test (or both) after the word stable in the commands below. Learn about nightly and test channels.

 `echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null`
## Install Docker Engine
Update the apt package index, and install the latest version of Docker Engine and containerd, or go to the next step to install a specific version:

 `sudo apt-get update`
 `sudo apt-get install docker-ce docker-ce-cli containerd.io`

To install a specific version of Docker Engine, list the available versions in the repo, then select and install:

a. List the versions available in your repo:

 apt-cache madison docker-ce
b. Install a specific version using the version string from the second column, for example, 5:18.09.1~3-0~ubuntu-xenial.

 sudo apt-get install docker-ce=<VERSION_STRING> docker-ce-cli=<VERSION_STRING> containerd.io

here <VERSION_STRING> is :
 `5:20.10.12~3-0~ubuntu-focal`

+ Verify that Docker Engine is installed correctly by running the hello-world image.

 `sudo docker run hello-world`

This command downloads a test image and runs it in a container. When the container runs, it prints a message and exits.

Docker Engine is installed and running. The docker group is created but no users are added to it. You need to use sudo to run Docker commands. Continue to Linux postinstall to allow non-privileged users to run Docker commands and for other optional configuration steps.

## Upgrade Docker Engine

To upgrade Docker Engine, first run sudo apt-get update, then follow the installation instructions, choosing the new version you want to install.

## CMDS de base

+ liste CMDS : 

`docker`

+ liste les container
`docker ps`

+ lancer 1 container
`docker alpine`

tester l'image distroless plus légère

docker pull alpine:latest

lancer l'image
docker ps -a

d : detache (pr garder la main sur le conteneur)
avec son nom + quelle image
docker -di --name hugues

docker run

docker ps

docker ps -a (list & status des containeur)

+ pr se conectera 1 container
    + ti = titille wilde
    + nom du container 
    + sh = lancé avec 1 shell
docker exec -ti hugues sh

et on est ds le container
on a les cmd

->C'est quoi Dockerfile ?<-
=========

<br>
* fichier de configuration

<br>
* objectif : création d'une image

<br>
* séquence d'instructions : 
	- RUN : lancements de commandes (apt...)
	- ENV : variables d'environnement
	- EXPOSE : expositions de ports
	- VOLUME : défition de volumes
	- COPY : cp entre host et conteneur
	- ENTRYPOINT : processus maître
	- ...



-------------------------------------------------

-> Intérêts de dockerfile <-


* relancer une création d'image à tout moment

* meilleure visibilité sur ce qui est fait

* partage facile et possibilité de gitter

* script d'édition de docker file (variables...)

* ne pas se poser de question lors du docker run du conteneur

* création images prod // dev - CI // CD


-------------------------------------------------

-> Exemple de dockerfile <-

```
FROM ubuntu:latest
MAINTAINER xavier
RUN apt-get update \ 
&& apt-get install -y vim git \ 
&& apt-get clean \ 
&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* 
```


-> commande <-

```
docker build -t nomimage:version .
```


## docker swarm

## Créer une React App avec Docker

+ Prérequis:

    Installer NodeJS (avec NPM), version >= 8.10
    Installer Yarn
    Installer Docker

1. Créez un dossier de travail et y ouvrir un terminal;

npx create-react-app mon-app
cd mon-app
yarn start

2. Mettre l'application dans un container Docker.

2.1. créez un fichier Dockerfile à la racine du projet "react-app-test".

```yml
FROM node:latest

WORKDIR /app

COPY package.json /app

COPY yarn.lock /app

RUN yarn

COPY . /app

EXPOSE 3000

CMD ["yarn","start"]!
```

+ Vous avez alors la description d'une image qui est faite. 

+ Toujours dans le dossier, ouvrez un terminal et vous allez alors créer l'image avec Docker grâce à ce fichier Dockerfile (qui sera reconnu par Docker).

`docker image build . -t react-app-test:dev`
`d
+ On a donc créé une image nommée mon-app avec le tag dev, ce qui signifie que c'est l'image qui sera utilisée pour le développement. Vous pouvez voir vos images avec la commande :

docker images
Continuons ! Nous allons démarrer un containeur avec l'image fraichement créée :

docker container run -d -p 3000:3000 -v /home/arthur/Documents/GWT/mon-app/src:/app/src mon-app:dev
Après ça, Docker vous affichera l'identifiant de votre container, vous pourrez le voir en action avec la commande

docker ps
Pour agir sur votre container :

+ Arrêter un container:
docker stop [nom du container]
+ Supprimer un container 
docker rm -f [nom du container]